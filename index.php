<?php
	
	// require ('animal.php');
	// require ('ape.php');
	require ('frog.php');

	echo "Hewan 1 : <br>";
	$sheep = new Animal("Shaun");
	echo "Animal Name : $sheep->name <br>"; // "shaun"
	echo "Legs : $sheep->legs <br>"; // 2
	echo "Cold Blooded?: $sheep->cold_blooded <br><br>";// false  

	echo "Hewan 2 : <br>";
	$sungokong = new Ape("Kera Sakti");
	echo "Animal Name : $sungokong->name <br>";
	echo "Legs : $sungokong->legs <br>"; // 2
	echo "Cold Blooded?: $sungokong->cold_blooded <br>";
	echo "bunyi : $sungokong->yell<br><br>"; // "Auooo"

	echo "Hewan 3 : <br>";
	$kodok = new Frog("buduk");
	echo "Animal Name : $kodok->name <br>";
	echo "Legs : $kodok->legs <br>"; // 4
	echo "Cold Blooded?: $kodok->cold_blooded <br>";
	echo "Jump Sound : $kodok->jump" ; // "hop hop"

?>